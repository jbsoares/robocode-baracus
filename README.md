# Robot Baracus 

Robô desenvolvido com objetivo de avaliação em processo seletivo.

 

## Explicando um pouco do meu código!

> Meu robô foi projetado para buscar o oponente no campo e ir de encontro,com o objetivo de atingi-lo várias vezes com potência máxima.

#### Estatégia de movimentação inicial
* O mantenho se movendo para frente.
* Girando seu radar para escanear inimigos a direita e a esquerda.

#### Scanner
* Ao obter a distancia, energia e angulo do inimigo,meu robô decide por se virar e atirar ou virar, seguir em direção ao oponente e atirar.

#### Ao ser atingido
* Recebe a posição do inimigo e vai de encontro para atingi-lo.

#### Ao bater na parede 
* Gira a esquerda em 180 graus e escanea para encontrar oponentes próximos.

#### Ao bater em outro robô
* Gira no angulo do robô inimigo e atira.

### Autora 
Jade Soares.
