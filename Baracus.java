package baracus;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Baracus - a robot by (your name here)
 */
public class Baracus extends AdvancedRobot
{		
	/**
	 * run: Baracus's default behavior
	 */
	public void run() {
		// Initialization of the robot should be put here

		// After trying out your robot, try uncommenting the import at the top,
		// and the next line:

		setColors(Color.black,Color.yellow,Color.white); // body,gun,radar

		// Robot main loop
		while(true) {
			// Replace the next 4 lines with any behavior you would like
			ahead(40); //anda 40 pixels
			setTurnRadarRight(90); //gira o radar para direita em 90 graus
			setTurnRadarLeft(90); //gira o radar para esquerda em 90 graus
		}
	}

	/**
	 * onScannedRobot: What to do when you see another robot
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
	double angulo = e.getBearing(); //variável que recebe a direção do oponente em graus
	double distancia = e.getDistance(); //variável que recebe a distancia do oponente 
	double energia = e.getEnergy(); //variável recebe a energia do oponente
	
	if (distancia >= 200 && energia <= 16){ //estrutura condicional que tem como parametro distancia menor que 200 e energia menor que 16
		turnRight(angulo); //gira na direção do oponente
		fire(3); //atira com potencia 3
		}else if(distancia <= 100 && energia <= 10){ //condição a ser cumprida eh a distancia menor que 100 e energia menor igual a 10
			ahead (e.getDistance() + 5); //vai em direção ao oponente
			fire(3); //atira com potencia 3
		}
	}

	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		double enemie = e.getBearing(); //variável recebe o angulo da direção do inimigo em graus
		
		turnRight(e.getBearing()); //gira em direção ao inimigo
		ahead(enemie); //vai de encontro ao inimigo
		fire(3); //atira com potencia 2
	}
	
	/**
	 * onHitWall: What to do when you hit a wall
	 */
	public void onHitWall(HitWallEvent e) {
		// Replace the next line with any behavior you would like
		turnLeft(180); //gira para a esquerda em 180 graus
		scan(); //escanea
		
	}	
	public void onHitRobot(HitRobotEvent e) {
		turnRight(e.getBearing()); //gira para direita em angulo igual ao Bearing do inimigo
		ahead(90); //anda 90 pixels
		fire(3); //atira com potencia 3
	}

	}

